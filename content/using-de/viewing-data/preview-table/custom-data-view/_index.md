---
title: "Define default data views"
subtitle: 
comments: false
weight: 450
keywords: [
  'Default layout', '/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/default-layout/',
  'Increased table size', '/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/default-layout/increased-table-size',
  'Default filter selections', '/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/default-selection/',
  'Hide information of a data view', '/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/not-displayed/',
  'Implicit and explicit orders', '/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/implicit-explicit-order/',
  'Additional downloads of external resources', '/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/external-resources/',
  'Custom data alignment', '/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/custom-data-alignment/',
]

---

Further in these sub-sections are explained how one can customise a data view with the use of standardised SDMX Annotations and/or the business rules that illsutrate many statistical disseminations' use cases.

* [Default layout](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/default-layout/)
* [Increased table size](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/increased-table-size)
* [Default filter selections](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/default-selection/)
* [Hide information of a data view](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/not-displayed/)
* [Implicit and explicit orders](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/implicit-explicit-order/)
* [Reversed time period sort order](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/reversed-time-period/)
* [Additional downloads of external resources](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/external-resources/)
* [Custom data alignment](/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/custom-data-alignment/)
