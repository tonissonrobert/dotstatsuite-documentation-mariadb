---
title: "Customise table layout"
subtitle: 
comments: false
weight: 2650
keywords: [
  'General behavior', '#general-behavior',
  'Time Period reverse order', '#time-period-reverse-order',
  'Accessibility', '#accessibility',
]
---

#### Table of Content
- [General behavior](#general-behavior)
- [Time Period reverse order](#time-period-reverse-order)
- [Accessibility](#accessibility)

> This section describes how to customise the **table preview layout**. For the customisation of the chart views, please see [this section](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/charts/customise-feature/) of the documentation.

> *Version history:*  
> Time period default order can be overriden since [April 20, 2023 Release .Stat Suite JS unicorn](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#april-20-2023)  
> Replaced 'Customise' by 'Layout' for table with [April 20, 2023 Release .Stat Suite JS unicorn](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#april-20-2023)  
> Added a cross icon to close the panel with [December 5, 2022 Release .Stat Suite JS spin](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#december-5-2022)  

---

### General behavior
The DE user is able to customise the table preview by using the **"Layout"** option from the toolbar. It is thus possible to customise how the data is positioned in the table preview by moving (dragging and dropping) the available dimensions between the Columns, Rows and Row Sections axis.  
Upon clicking on "Layout", the following area is displayed (moving the table preview downwards):

![Customise](/dotstatsuite-documentation/images/de-customise-general-behaviour.png)

The **Drag & Drop** feature allows to:
* Move any dimension from one axis to another;
* Move any dimension from one position to another in the same axis (moving its position up or down).

**Exception**  
The `Rows` axis **cannot** be empty, otherwise the application cannot construct a data table grid. Thus, when there is only one dimension set to `Rows`, a **Forbidden** icon is displayed instead of the usual "hand" mouse icon.

**Table preview**  
The **Table preview** area automatically shows what the user modifies, allowing him to anticipate the changes in the table grid. The "Table preview" feature displays the corresponding localised dimension names in the correct location, but not the dimension values (replaced by `Xxxx`).

**Cancel/Apply**  
The modifications made in the "Layout" option are applied only when the user clicks on **Apply Layout**. When doing so, the "Layout" toolbar option is collapsed and the table preview layout is updated with the new modifications.  
Clicking on **Cancel changes** will set all dimensions back to the previous state, meaning as it was set when the user originally clicked on "Layout".  
Also, clicking on the top-right cross icon will close the the panel without applying any change.

---

### Time Period reverse order
All data displayed in the data table preview are shown with a Time Period (when defined is the Dataflow by the Data Producer). The default representation of the Time Period is:
* When Time Period is displayed in Column, it shows the oldest value on the left side, and the latest value on the right side;
* When Time Period is displayed in Row on Row Section, it shows the oldest value on the top of the table, and the latest value on the bottom of the table.

It is possible to change that representation in order to display the time period in **reverse mode**, so that the latest value comes first in any layout option.  
This option is managed by a selection box always embedded to the Time Period dimension in the "Layout" option:

![Time period reverse](/dotstatsuite-documentation/images/de-time-period-reverse1.png)

The option is always set to `Ascending` by default. When the user changes it to `Descending`, it automatically reverses the Time Period values accordingly.

![Time period reverse](/dotstatsuite-documentation/images/de-time-period-reverse2.png)

![Time period reverse](/dotstatsuite-documentation/images/de-time-period-reverse3.png)

**Note** that the default ascending order can be overriden at different levels:
* For a whole Data Explorer instance (see [configuration specs](https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/de-configuration/#time-period-sort-order-override))
* For all dataflows of a common DSD (see [functional specs](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/reversed-time-period/))
* For a single dataflow (see [functional specs](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/preview-table/custom-data-view/reversed-time-period/))

---

### Accessibility
As part of the [Web Content Accessibility compliance](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/general-layout/#web-content-accessibility-compliance), the user can also use **keyboard's shortcuts** instead of the Drag & Drop feature in order to customise the table layout.  
To do so, the user needs to focus the dimension she/he wishes to move using the `Tab` key. Once done, she/he can select it by clicking on  the `Space` bar. At this stage, she/he can use the `up`, `down`, `right` and `left` control arrow keys in order to move the selected item to the appropriate place in the same area, or in another axis ("Rows", "Columns", "Row sections").  
To unselect the item, she/he can click on the `Space` bar again.
