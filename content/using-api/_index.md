---
title: "Using .Stat Suite APIs"
subtitle: 
comments: false
weight: 4000

---
<!-- This page (or a sub-page or sub-section of this page) of the documentation is referenced as an external resource in the .Stat Academy:
* https://academy.siscc.org/using-dotstat-suite/
Any change affecting its URL must be communicated to the .Stat Academy content admin in advance. -->

![Core logo](/dotstatsuite-documentation/images/core_logo.png)

* [Main APIs features](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/api-main-features/)
* [.Stat Suite Core data model](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/core-data-model/)
* [Data features](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/data/)
* [Referential metadata features](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/ref-metadata/)
* [Typical use cases](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/typical-use-cases/)
* [.Stat SDMX RESTful Web Service Cheat Sheet](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/restful/)
* [Embargo management (Point in Time)](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/embargo-management/)
* [Confidentiality and embargo data](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/confidential-data/)
* [Email notifications for data management](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/message-through-mail/)
* [Programmatic authentication](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/programmatic-auth/)
* [Permission management](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/permission-management/)
