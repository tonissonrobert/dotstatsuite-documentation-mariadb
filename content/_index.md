> **Thank you for your interest in our project**. 
We welcome your interest and you can explore further to know if you would like to engage with our open source Community.  
You can find out more about the Community on our [web site](https://siscc.org). To help us continuously improve this documentation site, you can send suggestions by email to [contact@siscc.org](mailto:contact@siscc.org?subject=documentation-suggestion).

### What is .Stat Suite?

<div class="col-sm-8">

<div style="height:40px" aria-hidden="true"></div>

<div>
    <img decoding="async" src="/.stat-suite/design/demo/logos-svg/images/statsuite-base.svg" alt=".Stat Suite logo" style="width:285px;height:118px" width="285" height="118">
</div>
<p>The <strong>.Stat Suite</strong> is a standard-based, componentised, open-source platform for the efficient production and dissemination of high-quality statistical data. The product is based on the General Statistical Business Process Model (<a rel="noreferrer noopener" aria-label="GSBPM (opens in a new tab)" href="http://www1.unece.org/stat/platform/display/GSBPM/GSBPM+v5.0" target="_blank">GSBPM</a>) and the Statistical Data and Metadata eXchange (<a rel="noreferrer noopener" aria-label="SDMX (opens in a new tab)" href="https://sdmx.org" target="_blank">SDMX</a>) standards and is driven by the Statistical Information System Collaboration Community (<a rel="noreferrer noopener" aria-label="SIS-CC (opens in a new tab)" href="https://siscc.org" target="_blank">SIS-CC</a>). </p>

<p><br>The .Stat Suite has three&nbsp;<strong>main modules</strong>: </p>

<blockquote>
<div>
    <img decoding="async" loading="lazy" src="/.stat-suite/design/demo/logos-svg/images/dotstat-de-base.svg" alt=".Stat Suite Data Explorer logo" style="width:285px;height:97px" width="285" height="97">
</div>
<p>The <strong>.Stat Suite Data Explorer</strong>, a web-based front-office application for easy finding, understanding and using of data through an efficient well-tuned navigation and search approach, appropriate data previews and contextual metadata, and download in standard formats, APIs or share features. <a href="/dotstatsuite-documentation/using-de/">Read more…</a></p>
</blockquote>

<blockquote>
<div>
    <img decoding="async" loading="lazy" src="/.stat-suite/design/demo/logos-svg/images/dotstat-dlm-base.svg" alt=".Stat Suite Data Lifecycle Manager logo" style="width:285px;height:98px" width="285" height="98">
</div>
<p>The <strong>.Stat Data Lifecycle Manager</strong>, a set of adaptive back-office modules to efficiently and timely produce and (re-)use high quality statistical data by defining, running, automating, controlling and evaluating the underlying data processes. <a href="/dotstatsuite-documentation/using-dlm/">Read more…</a></p>
</blockquote>

<blockquote>
<div >
    <img decoding="async" loading="lazy" src="/.stat-suite/design/demo/logos-svg/images/dotstat-core-base.svg" alt=".Stat Suite CORE logo" style="width:285px;height:97px" width="285" height="97">
</div>
<p>The <strong>.Stat Core</strong>, a highly performing, secure SDMX-native Data Store based on standard protocols, to store and retrieve statistical data, structural and referential metadata, data process information and security settings. <a href="/dotstatsuite-documentation/using-api/">Read more…</a></p>
</blockquote>

<p></p>

<p>You can also check out a number of publicly available examples of the <a rel="noreferrer noopener" href="/dotstatsuite-documentation/about/powered-by/" data-type="URL" data-id="/dotstatsuite-documentation/about/powered-by/" target="_blank">.Stat Suite implementations</a>, and find out on the many features of the .Stat Suite from the <a rel="noreferrer noopener" href="/dotstatsuite-documentation/about/product-overview/" data-type="URL" data-id="/dotstatsuite-documentation/about/product-overview/" target="_blank">detailed product overview</a> and the <a rel="noreferrer noopener" href="/dotstatsuite-documentation/about/product-overview/#flight-planner" data-type="URL" data-id="/dotstatsuite-documentation/about/flight-planner/" target="_blank">flight planner</a>.</p>
					
</div>

[![Mentioned in Awesome Official Statistics ](https://awesome.re/mentioned-badge.svg)](http://www.awesomeofficialstatistics.org)
