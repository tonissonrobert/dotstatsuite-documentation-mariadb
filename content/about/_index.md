---
title: "About"
subtitle: 
comments: false
weight: 10
keywords: [
  'License', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/license/',
  'Product overview', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/product-overview/',
  'Flight planner', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/flight-planner/',
  'Code of Conduct', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/code-of-conduct/',
  'Licenses of NuGet dependencies of .Stat Core module', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/dotstat-core-nuget-dependency-licenses/',
]
---

* [License](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/license/)
* [Product overview](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/product-overview/)
* [Flight planner](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/flight-planner/)
* [Powered by the SIS-CC .Stat Suite](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/powered-by/)
* [Code of Conduct](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/code-of-conduct/)
* [Licenses of NuGet dependencies of .Stat Core module](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/dotstat-core-nuget-dependency-licenses/)
